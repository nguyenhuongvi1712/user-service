import { ClassSerializerInterceptor, Injectable, UseInterceptors } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository, getRepository } from 'typeorm'
import { Roles } from './roles.entity'
import { Users } from './user.entity'
const bcrypt = require('bcrypt')
const jwk = require('../../util/jwt-helper.js')
const SALT_ROUNDS = 10


@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(Users)
    private usersRepository: Repository<Users>,
    @InjectRepository(Roles)
    private rolesRespository: Repository<Roles>
  ) { }

  async findAll(): Promise<Users[]> {
    return this.usersRepository.find();
  }

  async findOne(id: number): Promise<Users> {
    return this.usersRepository.findOne({
      where: {
        id
      },
      relations: ["role"],
    })
  }

  async login(email: string, password: string) {

    const user = await this.usersRepository.findOne({
      select: ['email', 'id', 'role', 'username', 'password'],
      where: {
        email
      },
      relations: ["role"]
    })
    if (user.email) {
      if (bcrypt.compareSync(password, user.password)) {
        const accessTokenLife = process.env.ACCESS_TOKEN_LIFE || '0.05h'
        const accessTokenSecret = process.env.ACCESS_TOKEN_SECRET || 'abc12345678910JErIuyiBDUIwmWMnlAY9QbLzoa'
        const newUser = {email,username:user.username,role:user.role}
        const accessToken = await jwk.generateToken(newUser, accessTokenSecret, accessTokenLife)
        return {
          success: true,
          user:newUser,
          token: accessToken
        }
      }
    }
    return {
      success: false,

    }

  }

  async remove(id: number): Promise<void> {
    await this.usersRepository.delete(id);
  }
  @UseInterceptors(ClassSerializerInterceptor)
  async create(email: string, password: string, username: string, roleId: number) {
    const user = await this.usersRepository.findOne({
      where: { email }
    })
    if (user) {
      return {
        success: false,
        message: 'Email da ton tai'
      }
    } else {
      const hashPassword = bcrypt.hashSync(password, SALT_ROUNDS);
      const role = await this.rolesRespository.findOne({ where: { id: roleId } })
      const user = {
        username,
        password: hashPassword,
        email,
        role
      };
      const newUser = await this.usersRepository.save(user)
      if (!newUser) {
        return {
          success: false,
          message: 'Thu lai !'
        }
      } else {
        console.log(role)
        return {
          success: true,
          user: {
            username,
            email,
            role
          }
        }
      }
    }
  }
  async update(email: string, username: string, roleId: number) {
    const user = await this.usersRepository.findOne({ email })
    if (user.id) {
      if (user.email === email && user.username === username && user.role.id === roleId)
        return {
          success: false,
          message: 'Nothing update'
        }
      user.email = email
      user.username = username
      user.role = await this.rolesRespository.findOne({ id: roleId })
      try {
        const updateUser = await this.usersRepository.save(user)
        return {
          success: true,
        }
      } catch (error) {
        console.log(error)
      }
    }
    return {
      success: false
    }
  }
}