import { Body, Controller, Get, Headers, Post, Put } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { LoginUserDto } from './dto/login-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Users } from './user.entity';
import { UsersService } from './users.service';
const jwk = require('../../util/jwt-helper.js')
const DEFAULT_ROLE = 2
const ADMIN_ROLE = 1
const { TokenExpiredError } = require('jsonwebtoken');

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) { }
  @Get()
  async findAll(): Promise<Users[]> {
    return this.usersService.findAll()
  }

  @Post()
  async get(@Body() body) {
    const secretKey = process.env.ACCESS_TOKEN_SECRET || 'abc12345678910JErIuyiBDUIwmWMnlAY9QbLzoa'
    const decode = await jwk.verifyToken(body.token, secretKey)
    return this.usersService.findOne(decode.data.id)
  }
  @Post('/register')
  create(@Body() createUserDto: CreateUserDto) {
    let roleId
    if (createUserDto.roleId) {
      roleId = createUserDto.roleId
    } else {
      roleId = DEFAULT_ROLE
    }
    return this.usersService.create(createUserDto.email, createUserDto.password, createUserDto.username, roleId)
  }
  @Post('/login')
  login(@Body() loginUserDto: LoginUserDto) {
    return this.usersService.login(loginUserDto.email, loginUserDto.password)
  }
  @Get('verifyToken')
  async verifyToken(@Headers() header) {
    const token = header.authorization.split(' ')[1]
    const secretKey = process.env.ACCESS_TOKEN_SECRET || 'abc12345678910JErIuyiBDUIwmWMnlAY9QbLzoa'
    try {
      const decode = await jwk.verifyToken(token, secretKey)
      const user = await this.usersService.findOne(decode.data.id)
      if (user) {
        return ({
          user,
          'success': true,
          token
        })
      } else {
        return ({
          'success': false
        })
      }
    } catch (err) {
      if(err instanceof TokenExpiredError){
        
      }
      else
        console.log(err)
    }
  }
  @Put('/update')
  async update(@Body() updateUserDto: UpdateUserDto) {
    const { email, username, roleId } = updateUserDto
    return this.usersService.update(email, username, roleId)
  }
}