import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'
import { Users } from './user.entity';
import { Roles } from './roles.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Users, Roles])],
    controllers: [UsersController],
    providers: [UsersService]
})
export class UsersModule { }