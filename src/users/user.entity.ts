import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, Exclusion  } from 'typeorm';
import { Roles } from './roles.entity'
import { Exclude } from 'class-transformer';
@Entity()
export class Users {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  email: string;

  @Exclude()
  @Column({select : false})
  password: string;

  @ManyToOne(type => Roles, roles => roles.users)
  role: Roles

}